# Accounts

## GET /api/v1/accounts/:id

Returns an [Account](/entities/#Account)

## POST /api/v1/accounts/create

### Resource information

| Parameter               | Value |
|:------------------------|:------|
| Response format         | JSON  |
| Requires authentication | Yes   |
| Available since         | 0.0.1 |


### Parameters

| Name                    | Description                                        | Type          | Required |
|:------------------------|:---------------------------------------------------|:--------------|:---------|
| `username`              | username that the user wants to use as identifier  | String        | `True`   |
| `password`              | password to be used by the user                    | String        | `True`   |
| `password_confirmation` | repeat the user passwords. Should match `password` | String        | `True`   |
| `email`                 | email to be used by the user                       | String(Email) | `True`   |


## GET /api/v1/accounts/:id/followers

Return array of [Account](/entities/#Account)s that follows the user with the given id.

### Resource information

| Parameter               | Value                  |
|:------------------------|:-----------------------|
| Response format         | JSON                   |
| Requires authentication | Yes                    |
| Available since         | 0.0.1                  |
| Scope                   | `read` `read:accounts` |

### Parameters

| Name    | Description               | Type            | Required | Default | Included in |
|:--------|:--------------------------|:----------------|:---------|:--------|-------------|
| `limit` | Maximum number of results | Number(Integer) | `False`  | 40      | 0.0.1       |


### Response

| Name        | Description              | Type        | Nullable |
|:------------|:-------------------------|:------------|:---------|
| `followers` | Array of `Accounts`      | Array       | `False`  |
| `next`      | Url to the next page     | String(URL) | `True`   |
| `previous`  | Url to the previous page | String(URL) | `True`   |

## GET /api/v1/accounts/:id/following

Return array of [Account](/entities/#Account)s that the user is following.

### Resource information

| Parameter               | Value                  |
|:------------------------|:-----------------------|
| Response format         | JSON                   |
| Requires authentication | Yes                    |
| Available since         | 0.0.1                  |
| Scope                   | `read` `read:accounts` |

### Parameters

| Name    | Description               | Type            | Required | Default |
|:--------|:--------------------------|:----------------|:---------|:--------|
| `limit` | Maximum number of results | Number(Integer) | `False`  | 40      |

### Response

| Name        | Description              | Type        | Nullable |
|:------------|:-------------------------|:------------|:---------|
| `followers` | Array of `Accounts`      | Array       | `False`  |
| `next`      | Url to the next page     | String(URL) | `True`   |
| `previous`  | Url to the previous page | String(URL) | `True`   |


## GET /api/v1/accounts/:id/statuses

Return array of [Status](/entities/#Status)es created by the user.

### Resource information

| Parameter               | Value                  |
|:------------------------|:-----------------------|
| Response format         | JSON                   |
| Requires authentication | Yes                    |
| Available since         | 0.0.1                  |
| Scope                   | `read` `read:statuses` |

### Parameters

| Name    | Description               | Type            | Required | Default |
|:--------|:--------------------------|:----------------|:---------|:--------|
| `limit` | Maximum number of results | Number(Integer) | `False`  | 40      |
|         |                           |                 |          |         |

### Response

| Name        | Description              | Type        | Nullable |
|:------------|:-------------------------|:------------|:---------|
| `followers` | Array of `Accounts`      | Array       | `False`  |
| `next`      | Url to the next page     | String(URL) | `True`   |
| `previous`  | Url to the previous page | String(URL) | `True`   |

