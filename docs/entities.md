# Entities

## Atachment

|  Attribute  |     Type     | Nullable | Added in |
| ----------- | ------------ | -------- | -------- |
| id          | String       | False    | 0.0.1    |
| type        | String       | False    | 0.0.1    |
| description | String       | False    | 0.0.1    |
| url         | String (URL) | False    | 0.0.1    |
| preview     | String (URL) | False    | 0.0.1    |
| meta        | Hash         | True     | 0.0.1    |


## Account

| Attribute       | Type        | Nullable | Added in |
|:----------------|:------------|:--------:|:--------:|
| id              | String      | False    | 0.0.1    |
| username        | String      | False    | 0.0.1    |
| acct            | String      | False    | 0.0.1    |
| display_name    | String      | False    | 0.0.1    |
| locked          | String      | False    | 0.0.1    |
| created_at      | String      | False    | 0.0.1    |
| followers_count | Number      | False    | 0.0.1    |
| following_count | Number      | False    | 0.0.1    |
| statuses_count  | Number      | False    | 0.0.1    |
| note            | String      | False    | 0.0.1    |
| url             | String(URL) | False    | 0.0.1    |
| avatar          | String(URL) | False    | 0.0.1    |
| header          | String(URL) | False    | 0.0.1    |
| avatar_static   | String(URL) | False    | 0.0.1    |
| header_static   | String(URL) | False    | 0.0.1    |
| moved           | Account     | True     | 0.0.1    |
| fields          | Array[Hash] | True     | 0.0.1    |
| bot             | Boolean     | True     | 0.0.1    |
| account_type    | String      | False    | 0.0.1    |
